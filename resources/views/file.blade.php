<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Test</title>
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
      <style>
         .border-left-blue{
         border-left:2px solid blue;
         }
      </style>
   </head>
   <body>
      <div class="row" >
         @if ($message = Session::get('message'))
         <div class="alert alert-info alert-block">
            <strong>{{ $message }}</strong>
         </div>
         @endif
         <div class="col-md-4" style="background: #eee;">
            <div class="col-md-12 border-bottom mb-2  p-2">
               <div class="row">
                  <div class="col-md-3">
                     Files
                  </div>
                  <div class="col-md-9">
                     <form action="{{route('files.store')}}" id="pdf_form" method="post" enctype="multipart/form-data">
                        <div class="input-group mb-3">
                           @csrf
                           <input type="file" class="form-control" placeholder="Choose Pdf file to upload" name="pdf_file" accept="application/pdf">
                           <span class="input-group-text"> <i class="fa fa-upload"></i></span>
                        </div>
                     </form>
                     @if ($errors->any())
                     <div class="alert alert-danger">
                        <ul>
                           @foreach ($errors->all() as $error)
                           <li>{{ $error }}</li>
                           @endforeach
                        </ul>
                     </div>
                     @endif
                  </div>
               </div>
            </div>
            <div class="col-md-12">
               @forelse($files as $file)
               <div class="bg-white border-left-blue border-bottom p-3 mb-2">
                  <div>
                     <a href="{{route('files.show',$file->id)}}">
                     Document #{{$loop->iteration}}
                     </a>
                  </div>
                  <div>
                     {{$file->name}}
                  </div>
               </div>
               @empty
               <div class="alert alert-info">
                  {{__('file.nofilefound')}}
               </div>
               @endforelse
            </div>
         </div>
         <div class="col-md-8">
            <div class="col-md-12 bg-primary">
               <h1 class=" p-1">
                  Document #
               </h1>
            </div>
            <div style="min-height: 90Vh;">
               @if(isset($filePath))
               <iframe src="{{asset($filePath)}}#toolbar=0" width="100%" height="500px">
               </iframe>
               @else
               <div class="alert alert-info text-center">
                  {{__('file.nofilefound')}}
               </div>
               @endif
            </div>
         </div>
      </div>
   </body>
   <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
   <script type="text/javascript">
      $(document).ready(function() {
          $('input[name="pdf_file"]').change(function() {
              $('#pdf_form').submit();
            });
        });

   </script>
</html>
