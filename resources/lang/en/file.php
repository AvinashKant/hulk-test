<?php

return [

    'nofilefound' => 'No record found',
    'success' => 'You have successfully upload file.',
    'error' => 'There are some error while processing your request.',

];
