NOTE: All the codes are present in their respective folder.

## Step 1: Setup app

Use this command to clone project in local system

git clone https://gitlab.com/AvinashKant/hulk-test

After cloning git repository go to project directory cd hulk-test

run : 'composer install' in cmd

## Step 2: Run database

1. Create a new database 'hulk-test' in your local db
2. Set your local db details in .env files
3. php artisan migrate

## Step 3: Run local server

php artisan serve

## Step 4: Create symbolic link

php artisan storage:link

## Step 4: Test Flow

1. Copy this url and paste into URL and press enter
http://127.0.0.1:8000/files



