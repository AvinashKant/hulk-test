<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class PdfFile extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = ['name', 'file_name'];

    public function getFileNameAttribute($value)
    {
        return Storage::url('public/files/' . $value);

    }
}
